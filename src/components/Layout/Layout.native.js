//dependencies
import React, { useState } from 'react';
import { Text, View, SafeAreaView, ScrollView, StyleSheet, } from 'react-native';

//files
import styles from '../../styleMain';
import SideMenu from '../SideMenu/SideMenu';
import Header from '../Header';
import Footer from '../Footer';
import UserPopUpMenu from '../UserPopUpMenu';

const Layout = (props) => {
	const [showSideDrawer, setShowSideDrawer] = useState(false);
	const [showProfileMenu, setProfileMenu] = useState(false);

	const toggleSideMenu = () => {
		console.log('pressed')
		setShowSideDrawer(!showSideDrawer);
	};

	const toggleProfileMenu = () => {
		setProfileMenu(!showProfileMenu)
	}

	return (
		<SafeAreaView style={styles.scrollView}>
			<ScrollView
				contentInsetAdjustmentBehavior="automatic"
				style={styles.scrollView}
				contentContainerStyle={styles.scrollView}>
				<View style={styles.container}>
					{showProfileMenu && <UserPopUpMenu />}
					{showSideDrawer ? <SideMenu toggleSideMenu={toggleSideMenu} /> : null}
					<View style={styles.col2}>
						<Header toggleSideMenu={toggleSideMenu} toggleProfileMenu={toggleProfileMenu}/>
						{props.children}
					</View>
				</View>
			</ScrollView>
		</SafeAreaView>
	)
};

export default Layout;
