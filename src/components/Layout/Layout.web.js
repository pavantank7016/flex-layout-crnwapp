//dependencies
import React, { useState } from 'react';
import { Text, View, SafeAreaView, ScrollView, } from 'react-native';

//files
import styles from '../../styleMain';
import SideMenu from '../SideMenu/SideMenu';
import Header from '../Header';
import Footer from '../Footer';
import Right from '../Right';
import UserPopUpMenu from '../UserPopUpMenu';

const Layout = (props) => {
	const [showSideDrawer, setShowSideDrawer] = useState(true);
	const [showProfileMenu, setProfileMenu] = useState(false);

	const toggleSideMenu = () => {
		setShowSideDrawer(!showSideDrawer);
	};

	const toggleProfileMenu = () => {
		setProfileMenu(!showProfileMenu)
	}

	return (
		<SafeAreaView style={styles.scrollView}>
			<ScrollView
				contentInsetAdjustmentBehavior="automatic"
				style={styles.scrollView}
				contentContainerStyle={styles.scrollView}>
				<View style={styles.container}>
					{showProfileMenu && <UserPopUpMenu />}
					<SideMenu isSideMenuOpen={showSideDrawer} toggleSideMenu={toggleSideMenu}/>
					<View style={styles.col2}>
						<Header toggleSideMenu={toggleSideMenu} toggleProfileMenu={toggleProfileMenu} />
						{props.children}
						<Footer />
					</View>
					{/* <Right /> */}
				</View>
			</ScrollView>
		</SafeAreaView>
	)
};

export default Layout;
