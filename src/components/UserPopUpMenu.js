//dependencies
import React from 'react';
import { Text, View, TouchableOpacity, Image, StyleSheet, Platform } from 'react-native';

// images & icon
import UserIcon from '../assets/images/logo.png'
import WalletIcon from '../assets/icons/wallet.png'
import SettingsIcon from '../assets/icons/settings.png'
import LockIcon from '../assets/icons/lock.png'
import LogoutIcon from '../assets/icons/logout.png'

const UserPopUpMenu = () => {
  return (
    <View style={[styles.main, Platform.OS === 'web' ? styles.webStyle : styles.nativeStyle]}>
      <View style={styles.itemContainer}>
        <View style={styles.iconContainer}>
          <Image source={UserIcon} style={styles.userIcon} />
        </View>
        <Text style={styles.menuItemText}>View Profile</Text>
      </View>
      <View style={styles.itemContainer}>
        <View style={styles.iconContainer}>
          <Image source={WalletIcon} style={styles.icon} />
        </View>
        <Text style={styles.menuItemText}>My Wallet</Text>
      </View>
      <View style={styles.itemContainer}>
        <View style={styles.iconContainer}>
          <Image source={SettingsIcon} style={styles.icon} />
        </View>
        <Text style={styles.menuItemText}>Settings</Text>
      </View>
      <View style={styles.itemContainer}>
        <View style={styles.iconContainer}>
          <Image source={LockIcon} style={styles.icon} />
        </View>
        <Text style={styles.menuItemText}>Lock Screen</Text>
      </View>
      <View style={styles.itemContainer}>
        <View style={styles.iconContainer}>
          <Image source={LogoutIcon} style={styles.icon} />
        </View>
        <Text style={styles.menuItemText}>Logout</Text>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  main: {
    position: 'absolute',
    zIndex: 1111,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 1,
    elevation: 5
  },
  nativeStyle: {
    right: 5,
    left: 5,
    top: 60
  },
  webStyle: {
    right: 5,
    top: 60,
    width: '15vw'
  },
  itemContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5
  },
  iconContainer: {
    width: 35,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center'
  },
  userIcon: {
    height: 30,
    width: 30,
    borderRadius: 20,
  },
  icon: {
    height: 25,
    width: 25,
  },    
  menuItemText: {
    fontFamily: 'Poppins-ExtraLight',
    fontSize: 18,
    marginLeft: 10
  }
})

export default UserPopUpMenu;