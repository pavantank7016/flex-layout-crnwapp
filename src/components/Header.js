//dependencies
import React from 'react';
import { Text, View, TouchableOpacity, Image, StyleSheet, Platform } from 'react-native';

//files, icons & images
import { Icon } from 'react-native-elements'
import NotificationIcon from '../assets/icons/bell.png'
import UserIcon from '../assets/images/logo.png'

const Header = (props) => (
	<View style={styles.main}>
		<TouchableOpacity onPress={props.toggleSideMenu}>
			<Icon
				name="menu"
				size={30}
			/>
		</TouchableOpacity>
		<View style={styles.rightSection}>
			<Image source={NotificationIcon} style={styles.Icon} />
			<TouchableOpacity onPress={props.toggleProfileMenu}>
				<Image source={UserIcon} style={styles.userIcon} />
			</TouchableOpacity>
			{Platform.OS === 'web' && <Text style={styles.userText}>Admin</Text>}
		</View>
	</View>
);

const styles = StyleSheet.create({
	main: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		backgroundColor: 'yellow',
		padding: 10
	},
	rightSection: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	Icon: {
		height: 25,
		width: 25
	},
	userIcon: {
		height: 35,
		width: 35,
		borderRadius: 20,
		marginLeft: 20,
	},
	userText: {
		marginLeft: 5,
		fontFamily: 'Poppins',
	}
})

export default Header;
