//dependencies
import React, { useState } from 'react';
import { Text, View, StyleSheet, Platform, TouchableOpacity } from 'react-native';
import { useSpring, animated, config } from 'react-spring'


//files
import NavItems from './SideMenuItems'

const SideMenu = (props) => {
	const [flip, set] = useState(false)
	const AnimatedView = animated(View)
	const [animatedStyles, api] = useSpring(() => ({
		delay: 200,
		config: config.molasses,
	}))

	const toggleSideMenu = () => {
		if(props.isSideMenuOpen){
			props.toggleSideMenu()
			api.start({width: '-11vw'})
		}else{
			props.toggleSideMenu()
			api.start({width: '11vw'})
		}
	}

	return (
		<View style={[navStyles.sideMenu, !props.isSideMenuOpen && navStyles.closedMenuStyle]}>
			<AnimatedView style={animatedStyles}>
				<NavItems isSideMenuOpen={!props.isSideMenuOpen} toggleSideMenu={toggleSideMenu} />
			</AnimatedView>
		</View>
	)
};

const navStyles = StyleSheet.create({
	sideMenu: {
		height: '100vh',
		width: '15vw',
		padding: 10,
		borderRightColor: 'lightgrey',
		borderRightWidth: 1,
		backgroundColor: 'yellow'
	},
	closedMenuStyle: {
		width: '4vw',
	}
})
export default SideMenu;
