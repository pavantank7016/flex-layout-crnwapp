//dependencies
import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { Hoverable } from 'react-native-web-hover'
import { Icon } from 'react-native-elements'

const SideMenuItems = (props) => {
	const [isSubmenuVisible, setIsSubmenuVisible] = useState(false)
	const [ishovered, setIsHovered] = useState(false)

	useEffect(() => {
		if (ishovered && props.isSideMenuOpen) {
			setTimeout(() => {
				props.toggleSideMenu()
			}, 300);
		}
	}, [ishovered])

	return <View style={styles.navContainer}>
		<View style={styles.mainTitle}>
			<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenuTitle]}>Menu</Text>
		</View>
		<TouchableOpacity style={[styles.navItemStyle, styles.dropdown]} onPress={() => setIsSubmenuVisible(!isSubmenuVisible)}>
			<View style={styles.dropdownInner}>
				<Icon
					name="home"
					type="feather"
					size={24}
					style={styles.iconStyle}
				/>
				<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenu]}>Dashboards</Text>
			</View>
			<Icon
				name={!isSubmenuVisible ? 'right' : 'down'}
				type="antdesign"
				size={18}
				style={props.isSideMenuOpen && styles.closedMenu}
			/>
		</TouchableOpacity>
		{isSubmenuVisible && <View>
			<TouchableOpacity style={styles.navItemStyle}>
				<Text style={styles.sumMenu}>P</Text>
				<Text
					style={[styles.menuText, props.isSideMenuOpen && styles.closedMenu, styles.subNavItem]}>Project 1</Text>
			</TouchableOpacity>
			<TouchableOpacity style={styles.navItemStyle}>
				<Text style={styles.sumMenu}>P</Text>
				<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenu, styles.subNavItem]}>Project 2</Text>
			</TouchableOpacity>
			<TouchableOpacity style={styles.navItemStyle}>
				<Text style={styles.sumMenu}>P</Text>
				<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenu, styles.subNavItem]}>Project 3</Text>
			</TouchableOpacity>
		</View>}
		<View style={[styles.subTitle, props.isSideMenuOpen && styles.closedMenu]}>
			<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenuTitle]}>Apps</Text>
		</View>
		<Hoverable>
			{({ hovered }) => {
				hovered ? setIsHovered(true) : setIsHovered(false)
				return (
					<TouchableOpacity style={styles.navItemStyle} onPress={() => setIsHovered(true)}>
						<Icon
							name="calendar"
							type="feather"
							size={24}
							style={styles.iconStyle}
						/>
						<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenu]}>Calendar</Text>
					</TouchableOpacity>
				)
			}}
		</Hoverable>
		<Hoverable style={styles.navItemStyle}>
			<Icon
				name="file-text"
				type="feather"
				size={24}
				style={styles.iconStyle}
			/>
			<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenu]}>File Manager</Text>
		</Hoverable>
		<Hoverable style={styles.navItemStyle}>
			<Icon
				name="instagram"
				type="feather"
				size={24}
				style={styles.iconStyle}
			/>
			<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenu]}>Products</Text>
		</Hoverable>
		<Hoverable style={styles.navItemStyle}>
			<Icon
				name="mail"
				type="feather"
				size={24}
				style={styles.iconStyle}
			/>
			<Text style={[styles.menuText, props.isSideMenuOpen && styles.closedMenu]}>Inbox</Text>
		</Hoverable>
	</View>
};

const styles = StyleSheet.create({
	navContainer: {
		flex: 1
	},
	mainTitle: {
		marginTop: 40
	},
	subTitle: {
		paddingTop: 20,
	},
	sumMenu: {
		width: 25,
		textAlign: 'center',
		marginRight: 10
	},
	dropdown: {
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	dropdownInner: {
		flexDirection: 'row',
		alignItems: 'center'
	},
	iconStyle: {
		marginRight: 10
	},
	navItemStyle: {
		alignItems: 'center',
		flexDirection: 'row',
		paddingVertical: 10,
		width: '100%',
	},
	subNavItem: {
		fontSize: 13,
		color: 'grey'
	},
	menuText: {
		fontFamily: 'Poppins',
		fontSize: 15,
	},
	closedMenu: {
		display: 'none'
	},
	closedMenuTitle: {
		color: '#66000000',
	}
})

export default SideMenuItems;
