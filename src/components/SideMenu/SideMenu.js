//dependencies
import React from 'react';
import { Text, View, StyleSheet, Platform, TouchableOpacity, Animated } from 'react-native';

//files
import NavItems from './SideMenuItems';

const SideMenu = (props) => {
	return <View style={navStyles.main}>
		<View style={navStyles.inner}>
			<Animated.View style={navStyles.sideMenu}>
				<NavItems />
			</Animated.View>
			<TouchableOpacity style={navStyles.backDrop} onPress={props.toggleSideMenu}/>
		</View>
	</View>
};

const navStyles = StyleSheet.create({
	main: {
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		zIndex: 1000,
		flex: 1,
	},
	inner: {
		position: 'relative',
		flex: 1,
		flexDirection: 'row'
	},
	sideMenu: {
		height: '100%',
		width: '60%',
		padding: 10,
		backgroundColor: '#fff',
	},
	backDrop: {
		width: '35%'
	}
})

export default SideMenu;
