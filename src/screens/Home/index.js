// dependencies
import React from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';

//files
import mainStyles from '../../styleMain'
import Layout from '../../components/Layout/Layout'
import Card from '../../components/Card'

const App = () => {
  return (
    <Layout>
      <View style={mainStyles.mainContent}>
        <View style={mainStyles.article}>
          <Text style={mainStyles.bodyText}>Content</Text>
          <ScrollView>
            <View style={{
              flexDirection: 'row',
              flexWrap: 'wrap',
              alignItems: 'flex-start',
              justifyContent: 'center',
              alignContent: 'flex-start'
            }}>
              <View style={styles.cardContainer}>
                <Card i={0} />
              </View>
              <View style={styles.cardContainer}>
                <Card i={1} />
              </View>
              <View style={styles.cardContainer}>
                <Card i={2} />
              </View>
              <View style={styles.cardContainer}>
                <Card i={3} />
              </View>
              <View style={styles.cardContainer}>
                <Card i={4} />
              </View>
              <View style={styles.cardContainer}>
                <Card i={5} />
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </Layout>
  );
};
const styles = StyleSheet.create({
  cardContainer: {

    width: 350
  }
})
export default App;
